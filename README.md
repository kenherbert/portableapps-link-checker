A scraper to check the PortableApps app directory pages for broken links and links that can be upgraded to HTTPS.


## Usage

Just run `python linkchecker.py`.


## Command line options
`-v` or `--verbose` - Show verbose output
