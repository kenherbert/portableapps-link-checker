import os
import requests
from bs4 import BeautifulSoup
#from datetime import datetime
import argparse


parser = argparse.ArgumentParser(description='Check the PortableApps app directory web pages for broken and HTTPS upgradeable links')
parser.add_argument('-v', '--verbose', action='store_true',
                    help='show verbose output')

args = parser.parse_args()

show_output = args.verbose


base_url = 'https://portableapps.com'
extension = '/apps'

# First make sure we can access the page
response = requests.get(base_url + extension)

if not response:
    print('App directory not accessible, aborting')
else:
    print('App directory accessible. This will take a while....')
    soup = BeautifulSoup(response.text, 'html.parser')
    categories = soup.find_all('ul', class_='appdirectory')

    app_pages = []

#    for category in categories:
    if len(categories) > 0:
        category = categories[0]
#        print(category)
        full_category_title = category.previous_sibling.previous_sibling.get_text()
        category_title = full_category_title[0:full_category_title.find('(')].strip()

        if show_output:
            print('')
            print(f'Scanning category: {category_title}')

        app_links = category.find_all('a')
        
        for app_link in app_links:
            app_link_text = app_link.get_text().strip()
            app_href = app_link.get('href')
            app_page_link = base_url + app_href

            if show_output:
                print('')
                print(f'Checking page: {app_link_text}')

            app_page = requests.get(app_page_link)

            app_page_record = {
                'url': app_page_link,
                'name': False,
                'accessible': False,
                'broken_links': [],
                'upgradeable_links': [],
            }

            if app_page:
                app_page_record['accessible'] = True

                app_page_soup = BeautifulSoup(app_page.text, 'html.parser')
                app_page_content = app_page_soup.find(id='content')

                app_name = app_page_content.find(id='page-title').string.strip()
                app_page_record['name'] = app_name

                app_page_links = app_page_content.find_all('a', href=True)

                for link_tag in app_page_links:
                    href = link_tag.get('href')
                    link_text = link_tag.get_text().strip()
                    if show_output:
                        print(f'Evaluating link: {link_text}')

                    # Check absolute links for accessibility and upgradeability
                    if href.startswith('http'):
                        try:
                            link_test = requests.get(href)
                        except requests.exceptions.RequestException:
                            link_test = False

                        if not link_test:
                            app_page_record['broken_links'].append(href)
                        else:
                            if href.startswith('http://portableapps.com/'):
                                app_page_record['upgradeable_links'].append(href)
                            else:
                                if href.startswith('http://'):
                                    upgraded_href = href.replace('http:', 'https:', 1)

                                    try:
                                        upgraded_test = requests.get(upgraded_href)

                                        if upgraded_test:
                                            app_page_record['upgradeable_links'].append(href)
                                    except requests.exceptions.RequestException:
                                        #Do nothing because it isn't upgradeable
                                        pass
                    # Skip relative download links
                    elif href.startswith('/downloading/'):
                        if show_output:
                            print('Skipping download link')
                    # Check relative links for accessibility
                    else:
                        absolute_link = base_url + href
                        link_test = requests.get(absolute_link)

                        if not link_test:
                            app_page_record['broken_links'].append(absolute_link)


            else:
                if show_output:
                    print(f'Broken link: cannot process the page')


            if (not app_page_record['accessible']) or (len(app_page_record["broken_links"]) > 0) or (len(app_page_record['upgradeable_links']) > 0):
                app_pages.append(app_page_record)


    if show_output:
        print('')
        print('')
        print('')
        print('')

    #Output the final report
    if len(app_pages) > 0:
        for page in app_pages:
            if page['name']:
                print(page['name'])
            else:
                this_url = page['url']
                print(f'Page: {this_url}')
            print('')

            if not page['accessible']:
                print('Page link broken')
                print('')

            if len(page['broken_links']) > 0:
                print('Broken Links:')

                for broken_link in page['broken_links']:
                    print(broken_link)
    
                print('')

            if len(page['upgradeable_links']) > 0:
                print('Upgradeable Links:')

                for upgradeable_link in page['upgradeable_links']:
                    print(upgradeable_link)

                print('')

        print('')
    else:
        print('No issues found')                
